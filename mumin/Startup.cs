﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(mumin.Startup))]
namespace mumin
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
